package br.gov.caixa.sisra.bean;

import br.gov.caixa.sisra.model.service.IEntidade;

public class Cliente implements IEntidade {

	private Integer coCliente;
	private Integer tpGrupo;
	private String cpf;

	public Integer getCoCliente() {
		return coCliente;
	}

	public void setCoCliente(Integer coCliente) {
		this.coCliente = coCliente;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	@Override
	public Integer getId() {
		return this.coCliente;
	}

	public Integer getTpGrupo() {
		return tpGrupo;
	}

	public void setTpGrupo(Integer tpGrupo) {
		this.tpGrupo = tpGrupo;
	}

}
