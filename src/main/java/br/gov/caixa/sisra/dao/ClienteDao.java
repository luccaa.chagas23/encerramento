package br.gov.caixa.sisra.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

import br.gov.caixa.sisra.bean.Cliente;
import br.gov.caixa.sisra.conexao.Conexao;

public class ClienteDao implements ICrudDAO<Cliente> {

	private StringBuilder query;

	@Override
	public void save(Cliente... cliente) throws Exception {
		Conexao.abrir();
		Connection con = Conexao.getConection();
		query = new StringBuilder();
		query.append("INSERT INTO ");
		query.append("srasm009.sratb001_cliente ( ");
		query.append("nu_cpf, tp_grupo ) ");
		query.append("VALUES (");
		query.append(" ?, ? )");

		PreparedStatement insert = con.prepareStatement(query.toString());
		for (int i = 0; i < cliente.length; i++) {
			insert.setString(1, cliente[i].getCpf());
			insert.setInt(2, cliente[i].getTpGrupo());
			insert.addBatch();

			System.out.println(insert.toString());
		}
		insert.executeBatch();

		Conexao.fechar();

	}

	@Override
	public Cliente singleResult(Cliente param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Cliente> resultList(Cliente param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Cliente> resultList() {
		// TODO Auto-generated method stub
		return null;
	}

}
