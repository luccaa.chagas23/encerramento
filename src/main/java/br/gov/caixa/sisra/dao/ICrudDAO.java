package br.gov.caixa.sisra.dao;

import java.util.List;

import br.gov.caixa.sisra.model.service.IEntidade;

public interface ICrudDAO<E extends IEntidade>  {
	
	void save(E... ent) throws Exception;
	E singleResult(E param);
	List<E> resultList(E param);
	List<E> resultList();
	

}
