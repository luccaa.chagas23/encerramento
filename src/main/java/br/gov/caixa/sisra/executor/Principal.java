package br.gov.caixa.sisra.executor;

import java.security.Provider;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import br.gov.caixa.sisra.importacao.Importacao;
import br.gov.caixa.sisra.utils.ExternalProperties;

/**
 * @author Luccas Chagas
 *
 */
public class Principal {

	static {
		SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
		System.setProperty("current.time.date", format.format(new Date()));
	}

	private static final Logger LOG = Logger.getLogger(Provider.class);

	public static void main(String[] args) throws Exception {
		BasicConfigurator.configure();
		LOG.info("Lendo informacoes de cliente");
		try {
			Properties p = new Properties();
			ExternalProperties exProp = new ExternalProperties();
			p = exProp.getDiretorioArquivo();

			String dir = p.getProperty("dir.arquivo");
			Importacao.importarArquivos(dir);

		} catch (Exception e) {
			LOG.error(e.getMessage());
		}

	}

}
