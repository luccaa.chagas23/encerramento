/**
 * 
 */
package br.gov.caixa.sisra.utils;

import java.io.InputStream;
import java.util.Properties;

/**
 * @author Luccas Chagas
 *
 */
public class ExternalProperties {

	public Properties getConenctionProperties() throws Exception {
		String arquivoProperties = "connection.properties";
		Properties p = new Properties();

		InputStream f = this.getClass().getClassLoader().getResourceAsStream(arquivoProperties);
		p.load(f);

		return p;

	}
	public Properties getDiretorioArquivo() throws Exception {
		Properties p = new Properties();

		InputStream f = this.getClass().getClassLoader().getResourceAsStream("arquivosdir.properties");
		p.load(f);

		return p;

	}

}
