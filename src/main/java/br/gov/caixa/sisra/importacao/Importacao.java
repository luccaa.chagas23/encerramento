package br.gov.caixa.sisra.importacao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.gov.caixa.sisra.bean.Cliente;
import br.gov.caixa.sisra.dao.ClienteDao;
import br.gov.caixa.sisra.executor.Principal;

public class Importacao {

	private static final Logger LOG = Logger.getLogger(Principal.class);

	public static void importarArquivos(String dir) {
		ClienteDao clienteDao = new ClienteDao();
		Cliente cliente = null;
		Cliente[] clientes = null;
		File file = new File(dir);
		int index = 0;
		File[] files = file.listFiles();
		List<Cliente> listaCliente = new ArrayList<Cliente>();
		BufferedReader br = null;
		String cpf = "";
		try {
			LOG.info("Processando dados..");
			for (int i = 0; i < files.length; i++) {
				br = new BufferedReader(new FileReader(files[i]));
				while ((cpf = br.readLine()) != null) {
					cliente = new Cliente();
					cliente.setCpf(cpf);

					if (i == 0 || i == 1) {
						cliente.setTpGrupo(2);
					} else if (i == 2) {
						cliente.setTpGrupo(1);
					} else if (i == 3 || i == 4) {
						cliente.setTpGrupo(3);
					}
					listaCliente.add(cliente);
				}

			}
			clientes = new Cliente[listaCliente.size()];

			for (Cliente c : listaCliente) {
				clientes[index] = c;
				index++;
			}
			LOG.info("Salvando informacoes de cliente importados pelos arquivos..");
			clienteDao.save(clientes);
			LOG.info("Salvo com sucesso! Fim da execucao..");
		} catch (Exception e) {
			LOG.error(e.getMessage());

		}

	}

}
