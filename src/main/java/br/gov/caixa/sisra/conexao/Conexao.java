/**
 * 
 */
package br.gov.caixa.sisra.conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import br.gov.caixa.sisra.utils.ExternalProperties;

/**
 * @author Luccas Chagas
 *
 */
public class Conexao {

	private static String USER;
	private static String PASSWORD;
	private static String URL;

	private static final String DRIVER = "org.postgresql.Driver";
	private static Connection conn;


	public static Connection abrir() throws Exception {
		Class.forName(DRIVER);
		getProperties();
		conn = DriverManager.getConnection(URL, USER, PASSWORD);
		return conn;
	}

	private static void getProperties() throws Exception {
		Properties props = new Properties();
		ExternalProperties p = new ExternalProperties();

		props = p.getConenctionProperties();
		USER = props.getProperty("prop.database.user");
		PASSWORD = props.getProperty("prop.database.password");
		URL = props.getProperty("prop.database.url");

	}
	
	public static void fechar() throws Exception {
		conn.close();
	}
	
	public static Connection getConection() throws Exception {
		return conn;
	}

}
